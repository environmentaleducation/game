package com.example.quote_project;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class OnePlayer extends Activity implements OnClickListener {
	private int turnCount = 0;
	private Integer transScore= 0;
	private Integer energyScore= 0;
	private Integer waterScore= 0;
	private Integer recycleScore= 0;
	private Integer score = 0;
	private String answer = "";
	private String trans = "trans";
	private String energy = "energy";
	private String water = "water";
	private String recycle = "recycle";
	private String questionType = "";
	private ArrayList<quoteClass> quotes = new ArrayList<quoteClass>();
	Random generator = new Random();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one_player);
		addListeners();
		loadQuestionDB();
		loadQuestion();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main_menu, menu);
		return true;
	}

	public void addListeners() {
		TextView tv = (TextView) findViewById(R.id.score_textview);
		tv.setText("Player score: " + score);

		View choice1 = findViewById(R.id.choice1_button);
		choice1.setOnClickListener(this);
		View choice2 = findViewById(R.id.choice2_button);
		choice2.setOnClickListener(this);
		View choice3 = findViewById(R.id.choice3_button);
		choice3.setOnClickListener(this);
		View choice4 = findViewById(R.id.choice4_button);
		choice4.setOnClickListener(this);
	}

	public void loadQuestionDB() {
		{
			try {
				InputStream is = getResources().openRawResource(R.raw.quotedb);
				DataInputStream in = new DataInputStream(is);
				BufferedReader br = new BufferedReader(
						new InputStreamReader(in));
				String a1, a2, a3, a4, q, qt, key;

				while ((br.readLine()) != null) {
					q = br.readLine();
					a1 = br.readLine();
					a2 = br.readLine();
					a3 = br.readLine();
					a4 = br.readLine();
					key = br.readLine();
					qt = br.readLine();

					System.out.println(q + " " + a1 + " " + a2 + " " + a3 + " "
							+ a4 + " " + key + " " + qt);
					quoteClass temp = new quoteClass(q, a1, a2, a3, a4, key, qt);
					quotes.add(temp);
				}

				in.close();
			}

			catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}

	public void loadQuestion() {
		int rand = generator.nextInt(quotes.size());
		quoteClass temp = quotes.get(rand);

		while (temp.isChosen() == true) {
			rand = generator.nextInt(quotes.size());
			temp = quotes.get(rand);
		}

		temp.getQuestion();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.choice1_button:
			checkAnswer("a");
			break;
		case R.id.choice2_button:
			checkAnswer("b");
			break;
		case R.id.choice3_button:
			checkAnswer("c");
			break;
		case R.id.choice4_button:
			checkAnswer("d");
			break;
		}
	}

	public class quoteClass {
		private String question;
		private String answer1;
		private String answer2;
		private String answer3;
		private String answer4;
		private String keyValue;
		private String type;
		private Boolean hasBeenChosen;

		quoteClass() {
			question = "";
			answer1 = "";
			answer2 = "";
			answer3 = "";
			answer4 = "";
			keyValue = "";
			type ="";
			
			hasBeenChosen = false;
		}

		quoteClass(String q, String a1, String a2, String a3, String a4,
				String key, String qt) {
			question = q;
			answer1 = a1;
			answer2 = a2;
			answer3 = a3;
			answer4 = a4;
			keyValue = key;
			type = qt;
			hasBeenChosen = false;
		}

		public void getQuestion() {
			TextView questionBox = (TextView) findViewById(R.id.question_textview);
			TextView choice1 = (TextView) findViewById(R.id.choice1_button);
			TextView choice2 = (TextView) findViewById(R.id.choice2_button);
			TextView choice3 = (TextView) findViewById(R.id.choice3_button);
			TextView choice4 = (TextView) findViewById(R.id.choice4_button);

			questionBox.setText(question);
			choice1.setText(answer1);
			choice2.setText(answer2);
			choice3.setText(answer3);
			choice4.setText(answer4);

			answer = keyValue;
			questionType= type;
			hasBeenChosen = true;
		}

		public void resetChosen() {
			hasBeenChosen = false;
		}

		public boolean isChosen() {
			return hasBeenChosen;
		}
	}

	public static int getStringIdentifier(Context context, String name) {
		return context.getResources().getIdentifier(name, "raw",
				context.getPackageName());
	}

	public void checkAnswer(String check) {

		if (turnCount < 7) {
			if (check.equals(answer)) {
				new AlertDialog.Builder(this)
						.setTitle(R.string.correct)
						.setItems(R.array.returnToGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
									}
								}).show();
					if(questionType.equals (energy))
					{
						energyScore++;
					}
					else if(questionType.equals (trans))
					{
						transScore++;
					}
					else if(questionType.equals (water))
					{
						waterScore++;
					}
					else if(questionType.equals (recycle))
					{
						recycleScore++;
					}
				score++;

				TextView tv = (TextView) findViewById(R.id.score_textview);
				tv.setText("Player score: " + score);

				turnCount++;
				loadQuestion();
			} else {
				new AlertDialog.Builder(this)
						.setTitle(R.string.incorrect)
						.setItems(R.array.returnToGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
									}
								}).show();

				turnCount++;
				loadQuestion();
			}
		} else {
			if (check.equals(answer)) {
				score++;
				if(questionType.equals (energy))
				{
					energyScore++;
				}
				else if(questionType.equals (trans))
				{
					transScore++;
				}
				else if(questionType.equals (water))
				{
					waterScore++;
				}
				else if(questionType.equals (recycle))
				{
					recycleScore++;
				}
				String endScore = score.toString();
				

				new AlertDialog.Builder(this)
						.setTitle(
								"Correct Green Wise Kid! " + "Your final score is " + endScore)
						.setItems(R.array.endGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										restartGame(i);
									}
								}).show().setCanceledOnTouchOutside(false);

				TextView tv = (TextView) findViewById(R.id.score_textview);
				tv.setText("Player score: " + score);
			} else {
				String endScore = (score.toString());

				new AlertDialog.Builder(this)
						.setTitle(
								"Sorry try and be more GREEN! " + "Your final score is "
										+ endScore)
						.setItems(R.array.endGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										restartGame(i);
									}
								}).show().setCanceledOnTouchOutside(false);
			}
		}
	}

	public void restartGame(int i) {
		if (i == 0) {
			turnCount = 0;
			score = 0;
			energyScore= 0;
			transScore=0;
			waterScore=0;
			recycleScore=0;

			TextView tv = (TextView) findViewById(R.id.score_textview);
			tv.setText("Player score: " + score);

			for (int j = 0; j < quotes.size(); j++) {
				quoteClass temp = quotes.get(j);
				temp.resetChosen();
			}

			loadQuestion();
		} else if (i == 1) {
			Intent newgame = new Intent(this, MainMenu.class);
			startActivity(newgame);
		}
	}
}
