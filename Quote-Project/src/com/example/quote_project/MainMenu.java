package com.example.quote_project;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

public class MainMenu extends Activity implements OnClickListener {
	

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        addListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true;
    }
    
    public void addListeners()
    {
		View startButton = findViewById(R.id.start_button);
		startButton.setOnClickListener(this);
		View ruleButton = findViewById(R.id.about_button);
		ruleButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		case R.id.start_button:
			openNewGameDialog();
			break;
		case R.id.about_button:
			openAboutDialog();
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}
	
	private void openNewGameDialog() {
		new AlertDialog.Builder(this)
		.setTitle(R.string.new_game_title)
		.setItems(R.array.playerMode,
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialoginterface,
				int i) {
				startGame(i);
				}
				})
				.show();
	}
	
	private void openAboutDialog(){
		new AlertDialog.Builder(this).setTitle(R.string.about).setMessage(R.string.rules).show();
	}
	
	private void startGame(int i) {
		if(i == 0)
		{
			
			@SuppressWarnings("unused")
			Object school = 1;
			@SuppressWarnings("unused")
			Object grade = 0;
			Intent newgame = new Intent(this, OnePlayer.class);
			startActivity(newgame);
		}
		else if(i == 1)
		{
			
			@SuppressWarnings("unused")
			Object school = 1;
			@SuppressWarnings("unused")
			Object grade = 1;
			Intent newgame = new Intent(this, OnePlayer.class);
			startActivity(newgame);
		}
		else if(i == 2)
		{
			
			@SuppressWarnings("unused")
			Object school = 2;
			@SuppressWarnings("unused")
			Object grade = 0;
			Intent newgame = new Intent(this, OnePlayer.class);
			startActivity(newgame);
		}
		else if(i == 3)
		{
			
			@SuppressWarnings("unused")
			Object school = 2;
			@SuppressWarnings("unused")
			Object grade = 1;
			Intent newgame = new Intent(this, OnePlayer.class);
			startActivity(newgame);
		}
		else if(i == 4)
		{
			
			@SuppressWarnings("unused")
			Object school = 3;
			@SuppressWarnings("unused")
			Object grade = 0;
			Intent newgame = new Intent(this, OnePlayer.class);
			startActivity(newgame);
		}
		else if(i == 5)
		{
			
			@SuppressWarnings("unused")
			Object school = 3;
			@SuppressWarnings("unused")
			Object grade = 1;
			Intent newgame = new Intent(this, OnePlayer.class);
			startActivity(newgame);
		}
		
		
	}
}
	
